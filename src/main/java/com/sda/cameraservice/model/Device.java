package com.sda.cameraservice.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(callSuper = false, exclude = {"owner", "serviceOrders"})
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private Double value;

    @ManyToOne
    private AppUser owner;

    @OneToMany(mappedBy = "device", fetch = FetchType.EAGER)
    private Set<ServiceOrder> serviceOrders;
}

