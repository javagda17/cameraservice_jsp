package com.sda.cameraservice.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class ServiceOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @Lob
    private String content;

    @CreationTimestamp
    private LocalDateTime created;

    @Column(nullable = true)
    private LocalDateTime completed;

    @ManyToOne
    private Device device;
}
