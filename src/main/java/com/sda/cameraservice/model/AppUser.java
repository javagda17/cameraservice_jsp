package com.sda.cameraservice.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(callSuper = false, exclude = {"devices"})
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String username;

    private String password;

    private String firstName, lastName;

    private boolean isAdmin;

    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    private Set<Device> devices;
}
