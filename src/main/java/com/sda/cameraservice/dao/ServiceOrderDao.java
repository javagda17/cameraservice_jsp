package com.sda.cameraservice.dao;

import com.sda.cameraservice.model.AppUser;
import com.sda.cameraservice.model.Device;
import com.sda.cameraservice.model.ServiceOrder;
import lombok.extern.java.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.PersistenceException;
import java.util.Optional;
import java.util.logging.Level;

@Log
public class ServiceOrderDao {

    public static boolean addServiceOrder(ServiceOrder entity) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;

        try (Session session = sesssionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.save(entity);

            transaction.commit();
        } catch (PersistenceException re) {
            log.log(Level.SEVERE, "Error while adding device", re);
            if (transaction != null) {
                transaction.rollback();
            }
            return false;
        }
        return true;
    }

    public static Optional<ServiceOrder> getOrderById(Long id) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sesssionFactory.openSession()) {
            // stwórz zapytanie
            Query<ServiceOrder> query = session.createQuery("from ServiceOrder where id = :id", ServiceOrder.class);
            query.setParameter("id", id);

            return Optional.of(query.getSingleResult());
        } catch (PersistenceException pe) {
            log.log(Level.SEVERE, "Error while logging in", pe);
        }
        return Optional.empty();
    }

    public static boolean removeOrder(ServiceOrder order) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;

        try (Session session = sesssionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.delete(order);

            transaction.commit();
            return true;
        } catch (PersistenceException re) {
            log.log(Level.SEVERE, "Error while adding device", re);
            if (transaction != null) {
                transaction.rollback();
            }
            return false;
        }
    }
}
