package com.sda.cameraservice.dao;

import com.sda.cameraservice.model.AppUser;
import com.sda.cameraservice.model.Device;
import lombok.extern.java.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.PersistenceException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;

@Log
public class DeviceDao {
    public static boolean addDevice(Device entity) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;

        try (Session session = sesssionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.save(entity);

            transaction.commit();
        } catch (PersistenceException re) {
            log.log(Level.SEVERE, "Error while adding device", re);
            if (transaction != null) {
                transaction.rollback();
            }
            return false;
        }
        return true;
    }

//    public static Set<Device> deviceList(Long id) {
//        Optional<AppUser> appUserOptional = UserDao.getUserById(id);
//        if(appUserOptional.isPresent()){
//            AppUser user = appUserOptional.get();
//            return user.getDevices();
//        }
//
//        return new HashSet<>();
//    }

    public static Set<Device> deviceList(Long id) {
        return UserDao.getUserById(id).map(AppUser::getDevices).orElse(new HashSet<>());
    }

    public static Optional<Device> getDeviceById(Long id) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sesssionFactory.openSession()) {
            // stwórz zapytanie
            Query<Device> query = session.createQuery("from Device where id = :id", Device.class);
            query.setParameter("id", id);

            return Optional.of(query.getSingleResult());
        } catch (PersistenceException pe) {
            log.log(Level.SEVERE, "Error while logging in", pe);
        }
        return Optional.empty();
    }

    public static boolean removeDevice(Long id) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;

        Optional<Device> deviceOptional = getDeviceById(id);
        if (deviceOptional.isPresent()) {
            try (Session session = sesssionFactory.openSession()) {
                transaction = session.beginTransaction();

                Device device = deviceOptional.get();

                session.delete(device);

                transaction.commit();
                return true;
            } catch (PersistenceException re) {
                log.log(Level.SEVERE, "Error while adding device", re);
                if (transaction != null) {
                    transaction.rollback();
                }
                return false;
            }
        }
        return true;
    }
}
