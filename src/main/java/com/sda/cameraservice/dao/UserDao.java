package com.sda.cameraservice.dao;

import com.sda.cameraservice.model.AppUser;
import lombok.extern.java.Log;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.management.PersistentMBean;
import javax.persistence.PersistenceException;
import java.util.Optional;
import java.util.logging.Level;

@Log
public class UserDao {
    public static  Optional<AppUser> login(String username, String password) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sesssionFactory.openSession()) {
            // stwórz zapytanie
            Query<AppUser> query = session.createQuery("from AppUser where username = :username and password = :password", AppUser.class);
            query.setParameter("username", username);
            query.setParameter("password", password);

            return Optional.of(query.getSingleResult());
        } catch (PersistenceException pe) {
            log.log(Level.SEVERE, "Error while logging in", pe);
        }
        return Optional.empty();
    }

    public static boolean save(AppUser entity) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;

        try (Session session = sesssionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.save(entity);

            transaction.commit();
        } catch (PersistenceException re) {
            log.log(Level.SEVERE, "Error while register", re);
            if (transaction != null) {
                transaction.rollback();
            }
            return false;
        }
        return true;
    }

    public static  Optional<AppUser> getUserById(Long id) {
        SessionFactory sesssionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sesssionFactory.openSession()) {
            // stwórz zapytanie
            Query<AppUser> query = session.createQuery("from AppUser where id = :id", AppUser.class);
            query.setParameter("id", id);

            return Optional.of(query.getSingleResult());
        } catch (PersistenceException pe) {
            log.log(Level.SEVERE, "Error while logging in", pe);
        }
        return Optional.empty();
    }
}
