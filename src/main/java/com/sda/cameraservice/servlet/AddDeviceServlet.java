package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.DeviceDao;
import com.sda.cameraservice.dao.UserDao;
import com.sda.cameraservice.model.AppUser;
import com.sda.cameraservice.model.Device;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.sda.cameraservice.dao.UserDao.getUserById;

@WebServlet("/addDevice")
public class AddDeviceServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("addDevice.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String value = req.getParameter("value");

        // todo: weryfikacja - name nie może być pusty
        // todo: weryfikacja - value nie może być puste
        // jeśli jest błąd - wyświetlamy z powrotem addDevice i wyświetlamy komunikat.
        if (name == null || name.isEmpty() || value == null || value.isEmpty()) {
            resp.sendRedirect("/addDevice?error=");
            return;
        }

        // szukamy cookie z identyfikatorem użytkwonika.
        Cookie[] cookies = req.getCookies();
        Long userId = null;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login_info_id")) {
                userId = Long.parseLong(cookie.getValue());
                break;
            }
        }

        // użytkownik nie jest zalogowany bo nie ma cookie.
        if (userId == null) {
            resp.sendRedirect("/login");
            return;
        }

        Optional<AppUser> userOptional = UserDao.getUserById(userId);
        if (userOptional.isPresent()) {
            AppUser user = userOptional.get();

            Device device = new Device();
            device.setName(name);
            device.setValue(Double.parseDouble(value));
            device.setOwner(user);

            if(DeviceDao.addDevice(device)){
                user.getDevices().add(device);
                UserDao.save(user);

                // jeśli uda się poprawnie dodać urządzenie, to przekieruj na listę urządzeń
                resp.sendRedirect("/devices");
                return;
            }
        }else{
            // w razie błędu logowania przekieruj na logowanie
            resp.sendRedirect("/login");
            return;
        }

        // jeśli wystąpi inny błąd wyświetl błąd
        resp.sendRedirect("/addDevice?error=");
    }
}
