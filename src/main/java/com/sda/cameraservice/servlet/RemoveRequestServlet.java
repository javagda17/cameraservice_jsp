package com.sda.cameraservice.servlet;


import com.sda.cameraservice.dao.ServiceOrderDao;
import com.sda.cameraservice.model.ServiceOrder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/removeRequest")
public class RemoveRequestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idToRemove = req.getParameter("toRemove");
        Long id = Long.parseLong(idToRemove);

        // szukam request'u w bazie
        Optional<ServiceOrder> serviceOrderOptional = ServiceOrderDao.getOrderById(id);
        if (serviceOrderOptional.isPresent()) {
            // jeśli go odnajdę wyciągam go z optionala
            ServiceOrder serviceOrder = serviceOrderOptional.get();
            // następnie przekazuje go do usunięcia
            if (ServiceOrderDao.removeOrder(serviceOrder)) {
                // jeśli uda się usunąć przekierowuje na stronę 'details'

                resp.sendRedirect("/deviceDetails?identifier=" + serviceOrder.getDevice().getId());
                return;
            }
        }

        // jeśli cokolwiek pójdzie nietak, to przekierowuje na stronę z urządzeniami (bo nie mam id 'details' żeby
        // przekierować na tą stronę.
        resp.sendRedirect("/devices");
    }
}
