package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.UserDao;
import com.sda.cameraservice.model.AppUser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("login.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        Optional<AppUser> appUserOptional = UserDao.login(username, password);
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();

            Cookie cookieUsername = new Cookie("login_info_username", appUser.getUsername());
            Cookie cookieId = new Cookie("login_info_id", String.valueOf(appUser.getId()));

            cookieUsername.setMaxAge(60*5);
            cookieId.setMaxAge(60*5);

            resp.addCookie(cookieId);
            resp.addCookie(cookieUsername);

            resp.sendRedirect("/");
            return;
        }
        resp.sendRedirect("/login?error");
    }
}
