package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.DeviceDao;
import com.sda.cameraservice.dao.ServiceOrderDao;
import com.sda.cameraservice.model.Device;
import com.sda.cameraservice.model.ServiceOrder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/addRequest")
public class AddRequestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("identifier");
        Long identifier = Long.parseLong(id);

        req.setAttribute("deviceToWhichIAddRequest", identifier);
        RequestDispatcher dispatcher = req.getRequestDispatcher("addRequestForm.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String content = req.getParameter("content");
        String deviceId = req.getParameter("deviceId");

        Optional<Device> optionalDevice = DeviceDao.getDeviceById(Long.parseLong(deviceId));
        if (optionalDevice.isPresent()) {
            ServiceOrder order = new ServiceOrder();
            order.setContent(content);
            order.setDevice(optionalDevice.get());

            if(ServiceOrderDao.addServiceOrder(order)){
                resp.sendRedirect("/deviceDetails?identifier=" + deviceId);
                return;
            }
        }

        resp.sendRedirect("/deviceDetails?identifier=" + deviceId+ "&error=addOrderError");
    }
}
