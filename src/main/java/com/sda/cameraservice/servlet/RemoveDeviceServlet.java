package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.DeviceDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/removeDevice")
public class RemoveDeviceServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String identifier = req.getParameter("identifier");
        Long id = Long.parseLong(identifier);

        if(DeviceDao.removeDevice(id)){
            resp.sendRedirect("/devices");
            return;
        }
        resp.sendRedirect("/devices?error=removeError");
    }
}
