package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.DeviceDao;
import com.sda.cameraservice.model.Device;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/deviceDetails")
public class DeviceDetailsServlet extends HttpServlet {
    // parametry pochodzą z URL - służą przekazywaniu informacji do servletów z JSP/widoków...
    // argumenty pochdzą z servlet'ów i służą przekazywaniu informacji z servletów do jsp
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String identifier = req.getParameter("identifier");
        Long idDevice = Long.parseLong(identifier);

        Optional<Device> optionalDevice = DeviceDao.getDeviceById(idDevice);
        if (optionalDevice.isPresent()) {
            Device device = optionalDevice.get();

            req.setAttribute("deviceObject", device);
            req.setAttribute("orderList", device.getServiceOrders());

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("deviceDetails.jsp");
            requestDispatcher.forward(req, resp);
            return;
        }
        resp.sendRedirect("/devices");
    }
}
