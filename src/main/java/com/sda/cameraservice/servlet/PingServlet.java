package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.HibernateUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ping")
public class PingServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HibernateUtil.getSessionFactory();

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pong.jsp");
        requestDispatcher.forward(req, resp);
    }
}
