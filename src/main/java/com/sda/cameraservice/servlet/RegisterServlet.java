package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.UserDao;
import com.sda.cameraservice.model.AppUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password= req.getParameter("password");
        String passwordConfirm= req.getParameter("confirm-password");
        if(password.equals(passwordConfirm)) {
            AppUser user = new AppUser();
            user.setUsername(username);
            user.setPassword(password);

            Boolean success = UserDao.save(user);
            if (success) {
                resp.sendRedirect("/login");
                return;
            }
        }
        resp.sendRedirect("/login?registerError=error");
    }
}
