package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.DeviceDao;
import com.sda.cameraservice.model.Device;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/editDevice")
public class EditDeviceServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String edited = req.getParameter("editedId");
        Long id = Long.parseLong(edited);

        Optional<Device> optionalDevice = DeviceDao.getDeviceById(id);
        if (optionalDevice.isPresent()) {
            Device device = optionalDevice.get();

            req.setAttribute("deviceToEdit", device);

            RequestDispatcher dispatcher = req.getRequestDispatcher("addDevice.jsp");
            dispatcher.forward(req, resp);
        }
        resp.sendRedirect("/devices");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
