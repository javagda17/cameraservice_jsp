package com.sda.cameraservice.servlet;

import com.sda.cameraservice.dao.DeviceDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/devices")
public class DevicesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // na stronie musi się pojawić lista urządzeń.

//        <a class="btn btn-outline-primary" href="/addDevice"> Add device </a>

        // 1. stworzyć jsp  (użyć jsp:include aby wyświetlały się nagłówek i stopka
        // 2. w jsp powinien się pojawić guzik który przekieruje nas na strone /addDevice -
        // 3. stwórz servlet 'addDevice' oraz jsp do tego servletu
        // 4. stwórz w servlecie metodę doGet oraz metodę doPost
        // 5. po poprawnym dodaniu urządzenia (w metodzie doPost) przekieruj użytkownika na stronę /devices
        // 5. w razie błędu wróć (sendRedirect) na stronę 'addDevice' z komunikatem błędu

        // kopia wybrania użytkownika z addDevice
        Cookie[] cookies = req.getCookies();
        Long userId = null;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("login_info_id")) {
                userId = Long.parseLong(cookie.getValue());
                break;
            }
        }

        req.setAttribute("deviceList", DeviceDao.deviceList(userId));

        RequestDispatcher dispatcher = req.getRequestDispatcher("devices.jsp");
        dispatcher.forward(req, resp);
    }
}
