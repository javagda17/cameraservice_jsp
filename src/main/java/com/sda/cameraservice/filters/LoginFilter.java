package com.sda.cameraservice.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = ((HttpServletRequest) request);

            String url = httpRequest.getRequestURL().toString();
            String queryString = ((HttpServletRequest) request).getQueryString();
            if (url.contains("/static/") || url.endsWith("/login") || url.endsWith("/index") || url.endsWith("/")) {
                // jeśli strona to login lub index
                // to nic nie rób
                chain.doFilter(request, response);
                return;
            }

            Cookie username = null;
            Cookie[] cookies = httpRequest.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("login_info_username")) {
                    username = cookie;
                    break;
                }
            }
            if (username == null) {
                //    Jeśli nie jestem zalogowany (username == null) to przekieruj mnie na stronę logowania
                HttpServletResponse httpServletResponse = (HttpServletResponse) response;

                httpServletResponse.sendRedirect("/login");
                return;
            }
        }


        // koniecznie przekazujemy chain.doFilter jeśli chcemy aby strona ładowała się dalej
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
