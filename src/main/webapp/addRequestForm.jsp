<html>
<head>
    <jsp:include page="fragments/headers.jsp"></jsp:include>
    <title>Add Request Form</title>
</head>
<body>
<jsp:include page="fragments/navigator.jsp"></jsp:include>

<%-- FORMULARZ --%>
<div class="container">
    <form action="/addRequest" method="post">
        <div class="row">
            <div class="offset-3 col-6">
                <h2>Request Form:</h2>
            </div>
        </div>
        <input type="hidden"  name="deviceId" value="<%out.print(request.getAttribute("deviceToWhichIAddRequest"));%>">
        <div class="row"> <%--wiersz --%>
            <div class="col-3">
                <label for="content">Content:</label>
            </div>
            <div class="col-7">
                <input id="content" class="form-control" name="content" type="text">
            </div>
        </div>
        <div class="row">
            <div class="offset-3 col-2">
                <input type="submit" value="Submit">
            </div>
        </div>
    </form>
</div>

<jsp:include page="fragments/footers.jsp"></jsp:include>
</body>
</html>
