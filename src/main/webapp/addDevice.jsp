<%@ page import="com.sda.cameraservice.model.Device" %>
<html>
<head>
    <jsp:include page="fragments/headers.jsp"></jsp:include>
    <title>Devices</title>
</head>
<body>
<jsp:include page="fragments/navigator.jsp"></jsp:include>

<%-- FORMULARZ --%>
<div class="container">
    <%Device toEdit = (Device) request.getAttribute("deviceToEdit");%>
    <form action="/addDevice" method="post">
        <div class="row">
            <div class="offset-3 col-6">
                <h2>Device Form:</h2>
            </div>
        </div>
        <div class="row"> <%--wiersz --%>
            <div class="col-3">
                <label for="name">Device name:</label>
            </div>
            <div class="col-7">

                <input id="name" class="form-control" name="name" type="text"
                       value="<% out.print(toEdit==null? "" : toEdit.getName()); %>">
            </div>
        </div>
        <div class="row"> <%-- wiersz --%>
            <div class="col-3">
                <label for="value">Device value:</label>
            </div>
            <div class="col-7">
                <input id="value" class="form-control" name="value" type="number" step="0.01"
                       value="<% out.print(toEdit==null? "" : toEdit.getValue()); %>">
            </div>
        </div>
        <div class="row">
            <div class="offset-3 col-2">
                <input type="submit" value="Submit">
            </div>
        </div>
    </form>
</div>

<jsp:include page="fragments/footers.jsp"></jsp:include>
</body>
</html>
