<%@ page import="com.sda.cameraservice.model.Device" %>
<%@ page import="java.util.Set" %>
<html>
<head>
    <jsp:include page="fragments/headers.jsp"></jsp:include>
    <title>Devices</title>
</head>
<body>
<jsp:include page="fragments/navigator.jsp"></jsp:include>

<div class="container">
    <div class="row">
        <a class="offset-8 col-2 btn btn-outline-primary" href="/addDevice"> Add device </a>
    </div>

    <div class="row header-row">
        <div class="col-2">Id</div>
        <div class="col-3">Name</div>
        <div class="col-3">Value</div>
        <div class="col-4"></div>
    </div>
    <%
        Set<Device> devices = (Set<Device>) request.getAttribute("deviceList");
        for (Device device : devices) {
            out.print("<div class=\"row table-row\">");
            out.print("<div class=\"col-2\">" + device.getId() + "</div>");
            out.print("<div class=\"col-3\">" + device.getName() + "</div>");
            out.print("<div class=\"col-3\">" + device.getValue() + "</div>");
            out.print("<div class=\"col-4 row\">");
            out.print("<a class=\"col-4 btn btn-outline-primary\" href=\"/removeDevice?identifier=" + device.getId() + "\"> Remove </a>\n");
            out.print("<a class=\"col-4 btn btn-outline-primary\" href=\"/editDevice?editedId=" + device.getId() + "\"> Edit </a>\n");
            out.print("<a class=\"col-4 btn btn-outline-primary\" href=\"/deviceDetails?identifier=" + device.getId() + "\"> Details </a>\n");
            out.print("</div>"); /*<-- tutaj wstawiamy linki*/
            out.print("</div>");
        }
    %>
</div>
<jsp:include page="fragments/footers.jsp"></jsp:include>
</body>
</html>
