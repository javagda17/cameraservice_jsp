<%@ page import="com.sda.cameraservice.model.Device" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.sda.cameraservice.model.ServiceOrder" %>
<%@ page import="java.util.List" %>
<html>
<head>
    <jsp:include page="fragments/headers.jsp"></jsp:include>
    <title>Device details</title>
</head>
<body>
<jsp:include page="fragments/navigator.jsp"></jsp:include>

<div class="container">
    <% Device device = (Device) request.getAttribute("deviceObject");%>
    <div class="row">
        <a class="offset-8 col-2 btn btn-outline-primary"
           href="/addRequest?identifier=<% out.print(device.getId()); %>"> Add request</a>
    </div>

    <div class="row header-row">
        <div class="col-4">Id:</div>
        <div class="col-8"><%out.print(device.getId());%></div>
    </div>
    <div class="row header-row">
        <div class="col-4">Name:</div>
        <div class="col-8"><%out.print(device.getName());%></div>
    </div>
    <div class="row header-row">
        <div class="col-4">Value:</div>
        <div class="col-8"><%out.print(device.getValue());%></div>
    </div>

    <div class="row offset-1 col-10">
        <% Set<ServiceOrder> serviceOrderList = (Set<ServiceOrder>) request.getAttribute("orderList"); %>
        <% for (ServiceOrder order : serviceOrderList) { %>
        <div class="row table-row col-12">
            <div class="col-1"><% out.print(order.getId());%></div>
            <div class="col-2"><% out.print(order.getCompleted());%></div>
            <div class="col-3"><% out.print(order.getCreated());%></div>
            <div class="col-2"><% out.print(order.getContent());%></div>
            <div class="col-2 row">
                <a class="col-12 btn btn-outline-primary"
                   href="/removeRequest?toRemove=<% out.print(order.getId()); %>">Remove</a>
            </div>
        </div>
        <% } %>
    </div>
</div>

<jsp:include page="fragments/footers.jsp"></jsp:include>
</body>
</html>
