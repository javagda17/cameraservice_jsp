<%
    Cookie username = null;
    Cookie[] cookies = request.getCookies();
    for (Cookie cookie : cookies) {
        if (cookie.getName().equals("login_info_username")) {
            username = cookie;
            break;
        }
    }
%>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">Super hiper computer store</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="/">Home</a>

        <%--// java --%>
        <%
            if (username != null) {
                out.print(username.getValue());
        %>
        <%-- --%>

        <a class="p-2 text-dark" href="/devices">Your devices</a>
    </nav>
    <a class="btn btn-outline-primary" href="/logout">Logout</a>
    <%--  --%>
    <% } else { %>
    <%-- --%>
    </nav>
    <a class="btn btn-outline-primary" href="/login">Login</a>
    <%-- --%>
    <%}%>
</div>